import time
import tm1637
from time import sleep, localtime
import config
from stoppablethread import StoppableThread


class Digits:
    """
    read the data and clock pin in the config file
    """
    def __init__(self):
        diop = config.pin["DIO"]
        clkp = config.pin["CLK"]
        self.tm = tm1637.TM1637Decimal(clk=clkp, dio=diop, brightness=1)
        self.thread = StoppableThread()
        self.alarm_on = False

    def display_hour(self):
        """
        Displays the input 'hour' ( a struct_time ) on the 4 segment displays
        """
        t = localtime()
        sleep(1 - time.time() % 1)

        if not self.alarm_on:
            self.tm.numbers(t.tm_hour, t.tm_min, True)
            sleep(.5)
            self.tm.numbers(t.tm_hour, t.tm_min, False)
        else:
            self.tm.numbers(t.tm_hour, t.tm_min, True)

    def update_alarmtime(self, alarm_time):
        self.alarm_time = alarm_time
        print(alarm_time)

    def setAlarmIndicator(self, alarm_on):
        """
        Enable or disable the alarm indicator
        """
        self.alarm_on = alarm_on

    def alarmmode(self):
        at = self.alarm_time

        self.tm.numbers(at.tm_hour, at.tm_min, True)
        sleep(.25)
        self.tm.write([0, 0, 0, 0])
        sleep(.25)

    def alarmsetting_display(self, alarm_time):
        """
        display the alarm time and blink the alarm led
        """
        self.alarm_time = alarm_time
        if self.thread.is_alive():
            self.thread.stop()
            self.thread.join()
        self.thread = StoppableThread()
        self.thread.set_action(self.alarmmode)
        self.thread.start()

    def default_display(self):
        """
        display the clock and the resume the led default state
        """
        if self.thread.is_alive():
            self.thread.stop()
            self.thread.join()
        self.thread = StoppableThread()
        self.thread.set_action(self.display_hour)
        self.thread.start()

if __name__ == '__main__':
    dig = Digits()
    dig.tm.brightness(0)
    #dig.display_hour()
    #encoded_string = dig.tm.encode_string("130.0")
    #dig.tm.write(encoded_string)
    dig.default_display()
    sleep(3)
    dig.alarmsetting_display(time.localtime(0))
    sleep(3)
    dig.update_alarmtime(localtime())
    sleep(3)
