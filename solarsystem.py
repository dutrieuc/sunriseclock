import logging
import math
import datetime
from math import cos, acos, sin, asin, tan, atan

import config
import light

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)

class SolarSystem:

    def __init__(self):
        self.latitude = math.radians(45)
        self.altitude = 0.5
        self.a0 = 0.1753475
        self.a1 = 0.7197
        self.k = 0.34542
        self.peak_color_temp = config.light["peak_temp"]
        self.space_irradiation = 1
        self.temp_transition = 0.08*180/math.pi
        self.declination = config.light["declination"]
        self.solar_noon = config.light["solar_noon"]

    def hra(hour, solar_noon):
        return math.radians(15*(hour - solar_noon))

    def declination_from_day(day):
        return - math.radians(-23.45) * math.cos((day+10) * 2 * math.pi/365.0)

    def elevation(hra, decl, lat):
        return asin(sin(decl)*sin(lat) + cos(decl)*cos(lat)*cos(hra))

    def hottel_irradiance(self, elevation):
        if(elevation < 0):
            return 0
        return self.space_irradiation \
             * (self.a0 + self.a1* math.exp(-self.k/cos(math.pi/2 - elevation)))

    def color_temp(self, elevation):
        return self.peak_color_temp*(1-math.exp(-self.temp_transition*elevation))

    def declination_from_daylength(self, daylength):
        declination = -cos(SolarSystem.hra(daylength/2, 12))/tan(self.latitude)
        declination = -atan(declination)
        return declination

    def day_from_daylength(daylength):
        declination = declination_from_daylength(daylength)
        earth_tilt = math.radians(23.45)
        if declination > earth_tilt:
            # winter solstice
            return -10
        if declination < -earth_tilt:
            # summer solstice
            return 172.5
        return asin(-declination/earth_tilt)*365/math.pi/2 + 81

    def set_position(self, declination, solar_noon):
        self.declination = declination
        self.solar_noon = solar_noon
        maxelevation = asin(cos(self.declination - self.latitude))
        self.space_irradiation = 1
        maxirr = self.hottel_irradiance(maxelevation)
        self.space_irradiation = 1.0 / maxirr

    def get_sun_char(self, hour):
        hra = SolarSystem.hra(hour, self.solar_noon)
        elevation = SolarSystem.elevation(hra, self.declination, self.latitude)
        irr = self.hottel_irradiance(elevation)
        color = self.color_temp(elevation)
        return (elevation, irr, color)

    def sunset(self, day = None):
        today = datetime.date.today()
        today = today - datetime.date(today.year, 1, 1)
        daynumber = today / datetime.timedelta(days=1)
        if day != None: daynumber = day
        declination = SolarSystem.declination_from_day(daynumber)
        solar_noon = config.light["solar_noon"]
        sunset = solar_noon + acos(tan(declination)*tan(self.latitude))*12/math.pi
        return sunset
        


if __name__ == '__main__':
    s = SolarSystem()
    l = light.Light.black_light()
    for h in range(14, 44):
        r = s.get_sun_char(h/2.0)
        print(h/2, r)
        l.turn_to_temp(r[2], r[1])
        print(l)
        print(l.getRGB())
    for d in range(0, 365):
        print(d, s.sunset(d))

