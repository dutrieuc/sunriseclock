
from enum import Enum

class Context(Enum):
    DEFAULT = 1
    ALARMSETTING = 2

class Action(Enum):
    SWITCH = 1
    RELEASE = 2
    COLOR_CHANGE = 3
    BRIGHT_CHANGE = 4
    PLAYPAUSE = 5
    JUMP3 = 6
    JUMP7 = 7
    FADE3 = 8
    FADE7 = 9
    QUICK = 10
    SLOW = 11
    AUTO = 12
    FLASH = 13
    DIY1 = 14
    DIY2 = 15
    DIY3 = 16
    DIY4 = 17
    DIY5 = 18
    DIY6 = 19
    # L_* events are thrown when holding the functions button
    # for some time
    L_PLAYPAUSE = 1005
    L_JUMP3 = 1006
    L_JUMP7 = 1007
    L_FADE3 = 1008
    L_FADE7 = 1009
    L_QUICK = 1010
    L_SLOW = 1011
    L_AUTO = 1012
    L_FLASH = 1013
    L_DIY1 = 1014
    L_DIY2 = 1015
    L_DIY3 = 1016
    L_DIY4 = 1017
    L_DIY5 = 1018
    L_DIY6 = 1019
    UNKNOWN = -1;
    NONE = 0;

    def long(self):
        return Action(self.value + 1000)

class Event:

    def __init__(self, action, color = None):
      self.action = action
      self.color = color

    def __str__(self):
      return "Event: type " + str(self.action) + ", color " + str(self.color)

    def set_color(self, color):
      self.color = color

