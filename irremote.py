"""
Prints the scan code of all currently pressed keys.
Updates on every keyboard event.
"""
import sys
from vectors import Vector
sys.path.append('..')
import keyboard
import zope.event
import event as evt
import config

def main():
    THROWN = "thrown"
    press_time = {}

    def keyboard_callback(e):
        if e.event_type == keyboard.KEY_DOWN:
            throw_event_press(e)
        if e.event_type == keyboard.KEY_UP:
            throw_long_press_event(e)

    def throw_event_press(e):
      event = evt.Event(evt.Action.NONE)
      brightness_mod = config.brightness_table.get(e.scan_code)
      color = config.color_table.get(e.scan_code)
      action = config.function_table.get(e.scan_code)
      if(color != None):
        event = evt.Event(evt.Action.COLOR_CHANGE)
        event.set_color(Vector.from_list(color).multiply(1/255.0))
      elif(brightness_mod != None):
        event = evt.Event(evt.Action.BRIGHT_CHANGE)
        event.set_color(Vector.from_list(brightness_mod))
      elif(action != None):
        action = action.upper()
        key = evt.Action[action]
        t = log_action(key, e.time)
        if(t != THROWN and t > config.long_press):
          press_time[key] = THROWN
          event = evt.Event(key.long())
      else:
        event = evt.Event(evt.Action.UNKNOWN)
      zope.event.notify(event)
      print(event)

    def log_action(key, timestamp):
      oldtime = press_time.get(key)
      if oldtime == THROWN:
        return THROWN
      if oldtime != None:
        return timestamp - oldtime
      press_time[key] = timestamp
      return 0

    def throw_long_press_event(e):
      action = config.function_table.get(e.scan_code)
      if(action != None):
        action = action.upper()
        key = evt.Action[action]
        oldtime = press_time.get(key)
        del press_time[key]
        if oldtime == THROWN :
            pass
        elif(oldtime != None and e.time - oldtime <= config.long_press):
          event = evt.Event(key)
          zope.event.notify(event)
          print(event)

    keyboard.hook(keyboard_callback)
    keyboard.wait()

if __name__ == '__main__':
  main()
