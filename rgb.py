from typing import NamedTuple
import math
class RGB(NamedTuple):
    red:int = 0
    green:int = 0
    blue:int = 0

    def WWhite():
        return RGB(1, 180.0/255, 107.0/255)
    def Red():
        return RGB(1, 0, 0)
    def Green():
        return RGB(0 ,1, 0)
    def Blue():
        return RGB(0, 0, 1)

    def gammaCorrect(self):
        gamma = 2.2
        return RGB(pow(self.red, gamma), pow(self.green, gamma), pow(self.blue, gamma))

    def temp_to_rgb(colour_temperature):
        #range check
        if colour_temperature < 1000:
            colour_temperature = 1000
        elif colour_temperature > 40000:
            colour_temperature = 40000

        tmp_internal = colour_temperature / 100.0

        # red
        if tmp_internal <= 66:
            red = 255
        else:
            tmp_red = 329.698727446 * math.pow(tmp_internal - 60, -0.1332047592)
            if tmp_red < 0:
                red = 0
            elif tmp_red > 255:
                red = 255
            else:
                red = tmp_red

        # green
        if tmp_internal <=66:
            tmp_green = 99.4708025861 * math.log(tmp_internal) - 161.1195681661
            if tmp_green < 0:
                green = 0
            elif tmp_green > 255:
                green = 255
            else:
                green = tmp_green
        else:
            tmp_green = 288.1221695283 * math.pow(tmp_internal - 60, -0.0755148492)
            if tmp_green < 0:
                green = 0
            elif tmp_green > 255:
                green = 255
            else:
                green = tmp_green

        # blue
        if tmp_internal >=66:
            blue = 255
        elif tmp_internal <= 19:
            blue = 0
        else:
            tmp_blue = 138.5177312231 * math.log(tmp_internal - 10) - 305.0447927307
            if tmp_blue < 0:
                blue = 0
            elif tmp_blue > 255:
                blue = 255
            else:
                blue = tmp_blue

        return  RGB(red/255.0, green/255.0, blue/255.0)

    def __str__(self):
        return "RBG: %s %s %s" % (self.red, self.green, self.blue)
