/**
   Reads X/Y values from a PS/2 mouse connected to an Arduino
   using the PS2Mouse library available from
     http://github.com/kristopher/PS2-Mouse-Arduino/
   Original by Kristopher Chambers <kristopher.chambers@gmail.com>
   Updated by Jonathan Oxer <jon@oxer.com.au>
*/

#include <PS2Mouse.h>
#include <Mouse.h>

#define MOUSE_DATA 10
#define MOUSE_CLOCK 9

#define RED 3
#define GREEN 5
#define BLUE 6

PS2Mouse mouse(MOUSE_CLOCK, MOUSE_DATA, STREAM);
bool idle;

const byte numChars = 32;
char receivedChars[numChars];
char tempChars[numChars];        // temporary array for use when parsing

// variables to hold the parsed data
int redv, greenv, bluev;

boolean newData = false;
/**
   Setup
*/
void setup()
{
  Serial.begin(19200);
  mouse.initialize();
  idle = true;
}

/**
   Main program loop
*/
void loop()
{
  int16_t data[3];
  mouse.report(data);
 
  if (data[0] != 8 || data[1] != 0 || data[2]!=0 || !idle){
    idle = data[0] == 8;
    switch ( data[0] ) {
      case 9:
        Mouse.press();
        break;
      case 10:
        Mouse.press(MOUSE_RIGHT);
        break;
      case 8:
        Mouse.release();
        Mouse.release(MOUSE_RIGHT);
      default:
        Mouse.move(data[1], -data[2]);
    }
  }

  recvWithStartEndMarkers();
  if (newData == true) {
      strcpy(tempChars, receivedChars);
          // this temporary copy is necessary to protect the original data
          //   because strtok() used in parseData() replaces the commas with \0
      parseData();
      driveLed();
      newData = false;
  }
}

//============

void recvWithStartEndMarkers() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;

    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
}

//============

void parseData() {      // split the data into its parts

    char * strtokIndx; // this is used by strtok() as an index

    strtokIndx = strtok(tempChars, ",");      // get the first part - the string
    redv = atoi(strtokIndx);     // convert this part to an integer
 
    strtokIndx = strtok(NULL, ","); // this continues where the previous call left off
    greenv = atoi(strtokIndx);     // convert this part to an integer

    strtokIndx = strtok(NULL, ",");
    bluev = atoi(strtokIndx);     // convert this part to a float

}

//============

void showParsedData() {
    Serial.println(redv);
    Serial.println(greenv);
    Serial.println(bluev);
    Serial.println(' ');
}

void driveLed()
{
  analogWrite(RED, redv);
  analogWrite(GREEN, greenv);
  analogWrite(BLUE, bluev);
}
