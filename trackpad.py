
import time
from select import select
from vectors import Vector
import evdev
import zope.event
import event as evt
import config

def main():

    def mouse_callback(e):
        step = config.light["trackpad_step"]
        x = Vector.from_list(config.mouse_x_vector).multiply(e[0]*step)
        y = Vector.from_list(config.mouse_y_vector).multiply(e[1]*step)
        event = evt.Event(evt.Action.BRIGHT_CHANGE)
        event.set_color(x.sum(y))
        zope.event.notify(event)

    def btn_callback(e):
        if(e.value == 0):
            zope.event.notify(evt.Event(evt.Action.SWITCH))


    device = evdev.InputDevice('/dev/input/event0')
    print(device)
   
    while True:
        vec = [0, 0]
        r, w, x = select([device], [], [])
        for event in device.read():
            if event.type == evdev.ecodes.EV_REL:
                vec[event.code] += event.value
            elif event.type == evdev.ecodes.EV_KEY:
                btn_callback(event)
        mouse_callback(vec)
        time.sleep(0.05)

if __name__ == '__main__':
  main()
